package com.example.alumnedam.quizgame;

/**
 * Created by acm on 5/04/18.
 */

public class Tema {
    private String id;
    private String descripcion;


    public Tema(String id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }
}
