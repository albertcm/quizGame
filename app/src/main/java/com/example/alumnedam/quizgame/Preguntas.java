package com.example.alumnedam.quizgame;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by acm on 5/04/18.
 */

public class Preguntas extends AppCompatActivity implements View.OnClickListener  {

    //FULL VARIABLES
    private RadioButton rbclick;
    private RadioButton rb1;
    private RadioButton rb2;
    private RadioButton rb3;
    private List<Pregunta> preguntas = new ArrayList<>();
    private List<Respuesta> respuestasTrue = new ArrayList<>();
    private String nombre;
    private int tema;
    private int numpregs;
    private int actualPreg = 0;
    private TextView tvPreg;
    private Button btnSiguiente;
    private int correctas = 0;
    private int incorrectas = 0;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_pregs);

        nombre = getIntent().getStringExtra("nombre");
        tema = getIntent().getIntExtra("tema", -1) + 1;
        String preguntasIn = getIntent().getStringExtra("preguntas");
        numpregs = Integer.parseInt(preguntasIn);
        tvPreg = (TextView) findViewById(R.id.tvPregunta);
        rb1 = (RadioButton) findViewById(R.id.rb1);
        rb2 = (RadioButton) findViewById(R.id.rb2);
        rb3 = (RadioButton) findViewById(R.id.rb3);

        //METHODS LLAMADA
        pregsList();
        respsList();
        listaRespostasActual();

        btnSiguiente = (Button) findViewById(R.id.btnNext);
        btnSiguiente.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btnNext) {
            TextView tvCorrectas = (TextView) findViewById(R.id.tvCorrectas);
            TextView tvIncorrectas = (TextView) findViewById(R.id.tvIncorrectas);
            RadioGroup rgRespuestas = (RadioGroup) findViewById(R.id.rgRespuestas);
            int selectedId = rgRespuestas.getCheckedRadioButtonId();

            rbclick = (RadioButton) findViewById(selectedId);

            if (respuestasTrue.get(actualPreg).getRespuesta().equals(rbclick.getText())) {
                Toast.makeText(this, "Correcta", Toast.LENGTH_SHORT).show();
                correctas++;
                tvCorrectas.setText(" " + correctas);
                actualPreg = actualPreg + 1;
                pregsList();
                respsList();
                listaRespostasActual();
            } else {
                Toast.makeText(this, "Incorrecta", Toast.LENGTH_SHORT).show();
                incorrectas++;
                tvIncorrectas.setText(" " + incorrectas);
            }
            if (correctas >= numpregs) {
                Intent intent = new Intent(this, Final.class);
                intent.putExtra("correctas", correctas);
                intent.putExtra("incorrectas", incorrectas);
                intent.putExtra("numpregs", numpregs);
                intent.putExtra("nombre", nombre);
                intent.putExtra("tema", tema);
                startActivity(intent);
            }
        }
    }

    private void pregsList() {

        Cursor c = MainActivity.db.rawQuery("SELECT id,enunciado,id_tema FROM Preguntas WHERE id_tema =" + tema, null);

        if (c.moveToFirst()) {
            do {
                Pregunta pregArr = new Pregunta(
                        c.getString(0),
                        c.getString(1),
                        c.getString(2)
                );
                preguntas.add(pregArr);
            } while (c.moveToNext());
        }

    }


    private void listaRespostasActual() {
        //respuestasTrue.clear();
        Cursor c = MainActivity.db.rawQuery("SELECT id,respuesta,correcta,id_pregunta FROM Respuestas WHERE correcta =1 and id_pregunta =" + preguntas.get(actualPreg).getId(), null);

        if (c.moveToFirst()) {
            do {
                Respuesta resArr = new Respuesta(
                        c.getString(0),
                        c.getString(1),
                        c.getString(2),
                        c.getString(3)
                );
                respuestasTrue.add(resArr);
            } while (c.moveToNext());
        }

    }

    private void respsList() {

        List<Respuesta> listResps = new ArrayList<>();
        Cursor c = MainActivity.db.rawQuery("SELECT id,respuesta,correcta,id_pregunta FROM Respuestas WHERE id_pregunta =" + preguntas.get(actualPreg).getId(), null);

        if (c.moveToFirst()) {
            do {
                Respuesta resArr = new Respuesta(
                        c.getString(0),
                        c.getString(1),
                        c.getString(2),
                        c.getString(3)
                );
                listResps.add(resArr);
            } while (c.moveToNext());
        }

        tvPreg.setText(preguntas.get(actualPreg).getEnunciado());

        rb1.setText(listResps.get(0).getRespuesta());

        rb2.setText(listResps.get(1).getRespuesta());

        rb3.setText(listResps.get(2).getRespuesta());

    }

}

