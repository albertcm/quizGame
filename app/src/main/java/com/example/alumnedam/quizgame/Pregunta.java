package com.example.alumnedam.quizgame;

/**
 * Created by acm on 5/04/18.
 */

 public class Pregunta {
        private String id, enunciado, id_tema;

        public Pregunta(String id, String enunciado, String id_tema) {
            this.id = id;
            this.enunciado = enunciado;
            this.id_tema = id_tema;
        }

        public String getId() {
            return id;
        }

        public String getEnunciado() {
            return enunciado;
        }

        public void setId(String id) {
            this.id = id;
        }
    }
