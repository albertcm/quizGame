package com.example.alumnedam.quizgame;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;


/**
 * Created by alumneDAM on 06/03/2018.
 */

public class SQLiteHelper extends SQLiteOpenHelper {
    private String sqlCreatePreguntas = "CREATE TABLE Preguntas (id INTEGER, enunciado TEXT, id_tema INTEGER, foreign key(id_tema) references Tema)";
    private String sqlCreateTema = "CREATE TABLE Tema (id INTEGER, descripcion TEXT)";
    private String sqlCreateRespuestas = "CREATE TABLE Respuestas (id INTEGER, respuesta TEXT, correcta BOOLEAN, id_pregunta INTEGER, foreign key(id_pregunta) references Preguntas)";
    private String sqlCreatePuntos = "CREATE TABLE Puntos (id INTEGER PRIMARY KEY AUTOINCREMENT,correctos INTEGER,incorrectos INTEGER,numpregs INTEGER,tema INTEGER)";

    public SQLiteHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(sqlCreatePreguntas);
        db.execSQL(sqlCreateTema);
        db.execSQL(sqlCreateRespuestas);
        db.execSQL(sqlCreatePuntos);
        insertar(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS Preguntas");
        db.execSQL("DROP TABLE IF EXISTS Tema");
        db.execSQL("DROP TABLE IF EXISTS Respuestas");
        db.execSQL("DROP TABLE IF EXISTS Score");
        db.execSQL(sqlCreatePreguntas);
        db.execSQL(sqlCreateTema);
        db.execSQL(sqlCreateRespuestas);
        db.execSQL(sqlCreatePuntos);
    }

    public void insertar(SQLiteDatabase db) {
        //TABLA TEMAS
        db.execSQL("INSERT INTO Tema (id,descripcion) VALUES (1,'Sumas') ");
        db.execSQL("INSERT INTO Tema (id,descripcion) VALUES (2,'Restas') ");
        //TABLA PREGUNTAS - SUMAS
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (1,'69+115',1) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (2,'8+33',1) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (3,'2+8+80',1) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (4,'2+2+45',1) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (5,'3+39',1) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (6,'19+98',1) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (7,'2+2',1) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (8,'20+25',1) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (9,'30+230',1) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (10,'2008+1115',1) ");
        //TABLA PREGUNTAS - RESTAS
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (11,'2-2',2) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (12,'3-25',2) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (13,'2-98',2) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (14,'222-98',2) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (15,'322-213',2) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (16,'522-142',2) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (17,'809-100',2) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (18,'90-85',2) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (19,'650-541',2) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (20,'685-511',2) ");
        //TABLA PREGUNTAS - MULTIPLICACIONES
        /*
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (21,'2x9',3) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (22,'5x4',3) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (23,'6x8',3) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (24,'50x4',3) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (25,'2x8',3) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (26,'5x5',3) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (27,'8x6',3) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (28,'25x3',3) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (29,'100x30',3) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (30,'9x0',3) ");
        //TABLA PREGUNTAS - DIVISIONES
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (31,'20/4',4) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (32,'300/25',4) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (33,'15/3',4) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (34,'22/2',4) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (35,'35/5',4) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (36,'151/3',4) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (37,'20/2',4) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (38,'90/5',4) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (39,'12/3',4) ");
        db.execSQL("INSERT INTO Preguntas (id,enunciado,id_tema) VALUES (40,'115/3',4) ");
        */
        //TABLA RESPUESTAS - SUMAS
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (1,184,1,1) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (2,169,0,1) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (3,204,0,1) ");

        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (5,76,0,2) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (6,41,1,2) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (7,43,0,2) ");

        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (9,77,0,3) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (10,89,0,3) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (11,90,1,3) ");

        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (13,51,0,4) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (14,41,0,4) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (16,49,1,4) ");

        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (17,42,1,5) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (18,41,0,5) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (19,39,0,5) ");

        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (21,119,0,6) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (22,117,1,6) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (23,115,0,6) ");

        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (25,76,0,7) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (26,41,1,7) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (27,43,0,7) ");

        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (29,43,0,8) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (30,41,0,8) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (31,45,1,8) ");

        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (33,43,0,9) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (34,41,0,9) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (36,50,1,9) ");

        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (37,3123,1,10) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (38,3213,0,10) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (39,3323,0,10) ");

        //TABLA RESPUESTAS - RESTAS
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (40,'1',0,11) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (41,'0',1,11) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (42,'3',0,11) ");

        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (43,'-22',1,12) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (44,'-23',0,12) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (45,'-24',0,12) ");

        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (46,'-96',1,13) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (47,'-78',0,13) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (48,'-97',0,13) ");

        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (49,'1124',0,14) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (50,'124',1,14) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (51,'1125',0,14) ");

        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (52,'125',0,15) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (53,'109',1,15) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (54,'225',0,15) ");

        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (55,'108',0,16) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (56,'380',1,16) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (57,'110',0,16) ");

        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (58,'381',0,17) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (59,'383',0,17) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (60,'709',1,17) ");

        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (61,'5',1,18) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (62,'6',0,18) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (63,'7',0,18) ");

        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (64,'-110',0,19) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (65,'-109',0,19) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (66,'109',1,19) ");

        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (67,'173',0,20) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (68,'174',1,20) ");
        db.execSQL("INSERT INTO Respuestas (id,respuesta,correcta,id_pregunta) VALUES (69,'170',0,20) ");

    }
}
