package com.example.alumnedam.quizgame;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by acm on 8/04/18.
 */

public class Final extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        setContentView(R.layout.activity_final);

        int correctas = getIntent().getIntExtra("correctas", -1);
        int incorrectas = getIntent().getIntExtra("incorrectas", -1);
        int numpregs = getIntent().getIntExtra("numpregs", -1);
        int tema = getIntent().getIntExtra("tema", -1);

        String nombre = getIntent().getStringExtra("nombre");
        TextView tvNombre = (TextView) findViewById(R.id.tvNombre);
        tvNombre.setText(nombre + " estos son tus resultados: ");
        TextView tvErronioss = (TextView) findViewById(R.id.tvIncorrectasFinal);
        tvErronioss.setText("Incorrectas: " + incorrectas);
        TextView tvAciertoss = (TextView) findViewById(R.id.tvCorrectasFinal);
        tvAciertoss.setText("Correctas: " + correctas);
        MainActivity.db.execSQL("INSERT INTO Puntos (correctos,incorrectos,numpregs,tema) VALUES (" + correctas + "," + incorrectas + "," + numpregs + "," + tema + ") ");

        //Volver al MENU
        Button bnVolver = (Button) findViewById(R.id.btnVolver);
        bnVolver.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}

