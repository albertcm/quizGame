package com.example.alumnedam.quizgame;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button start;
    private EditText etNombre;
    static SQLiteDatabase db;
    private List<Tema> listTemas = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        //RECOJO
        etNombre = findViewById(R.id.etNombre);
        start = findViewById(R.id.start);
        start.setOnClickListener(this);

        //METODOS
        crearBD();
        temas();
        minMaxNumPreg();
    }

    //CREAR BD
    private void crearBD() {
        SQLiteHelper usdbh =
                new SQLiteHelper(this, "DBQuizGame", null, 1);

        db = usdbh.getWritableDatabase();
    }

    //MIN MAX ETNUMBER
    private void minMaxNumPreg() {
        EditText et = findViewById(R.id.numPreguntas);
        et.setFilters(new InputFilter[]{new InputFilterMinMax("1", "10")});
    }

    //SPINNER TEMA
    private void rellenarSpinner() {
        Spinner spinner = findViewById(R.id.tema);
        String[] arrTemas = new String[listTemas.size()];
        int cont = 0;
        for (Tema tema : listTemas) {
            arrTemas[cont] = tema.getDescripcion();
            cont++;
        }
        ArrayAdapter<String> comboAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, arrTemas);
        spinner.setAdapter(comboAdapter);
    }

    private void temas() {
        Cursor c = db.rawQuery("SELECT id, descripcion FROM Tema", null);

        if (c.moveToFirst()) {
            do {
                String id = c.getString(0);
                String descripcion = c.getString(1);
                Tema nuevo = new Tema(
                        c.getString(0),
                        c.getString(1)
                );
                listTemas.add(nuevo);
            } while (c.moveToNext());
        }
        rellenarSpinner();
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.start) {

            Intent intent = new Intent(this, Preguntas.class);

            Spinner tema = (Spinner) findViewById(R.id.tema);
            EditText etNPreguntas = (EditText) findViewById(R.id.numPreguntas);
            if (etNombre.getText().toString().equals("")) {
                Toast.makeText(this, "El campo nombre no puede estar vacio", Toast.LENGTH_SHORT).show();
            } else {
                SharedPreferences prefs =
                        getSharedPreferences("MisPreferencias", Context.MODE_PRIVATE);

                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("nombre", etNombre.getText().toString());
                editor.commit();
                intent.putExtra("nombre", etNombre.getText().toString());
                intent.putExtra("tema", tema.getSelectedItemPosition());
                intent.putExtra("preguntas", etNPreguntas.getText().toString());

                startActivity(intent);
            }
        }


    }
}