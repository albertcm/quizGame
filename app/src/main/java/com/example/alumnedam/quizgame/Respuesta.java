package com.example.alumnedam.quizgame;

/**
 * Created by acm on 5/04/18.
 */

public class Respuesta {
    private String id, respuesta, correcta, id_pregunta;

    public Respuesta(String id, String respuesta, String correcta, String id_pregunta) {
        this.id = id;
        this.respuesta = respuesta;
        this.correcta = correcta;
        this.id_pregunta = id_pregunta;
    }

    public String getId() {
        return id;
    }

    public String getRespuesta() {
        return respuesta;
    }

    public void setId(String id) {
        this.id = id;
    }

}

